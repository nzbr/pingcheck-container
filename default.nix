{ pkgs ? import <nixpkgs> { } }:
pkgs.dockerTools.buildLayeredImage {
  name = "pingcheck";
  tag = "latest";

  contents = with pkgs; [
    busybox
    cacert
  ];

  config = {
    Cmd = [
      (pkgs.writeShellScript "init" ''
        set -eux
        ${pkgs.busybox}/bin/ln -s /etc/ssl/certs/ca-{bundle,certificates}.crt
        if ${pkgs.iputils}/bin/ping -c5 $TARGET; then
          ${pkgs.curl}/bin/curl $WEBHOOK
          exit 0
        else
          exit 1
        fi
    '')
    ];
  };
}
